<?php

namespace OpenbyteTestPackage;

class HelloWorldClass
{
    public function printHelloWorld(): void
    {
        print_r("Hello World with package version v1.0.1!");
    }
}